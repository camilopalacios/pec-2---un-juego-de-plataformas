# PEC 2 - Un juego de plataformas - Super Mario Bros. 1-1

Segunda práctica de la asignatura de programación en Unity 2D. Esta práctica reproduce el nivel 1-1 del juego Super Mario Bros.

## Descripción

Este juego requiere pocas presentaciones. Super Mario Bros. desarrollado y publicado por Nintendo es un icono de los juegos de plataformas y desde su publicación en 1985 ha tenido infinidad de secuelas. No es el primer juego de la saga de Mario pero probablemente sea el que más nostalgia genera entre sus jugadores.
El juego se desarrolla en un mundo formado por bloques y tuberías. Para este proyecto hemos recreado el primer nivel del juego. El nivel 1-1.

El objetivo de Mario es llegar al castillo de Bowser, al otro lado del escenario, donde Mario cree que tiene retenida a la princesa (Peach).

## Cómo jugar

Para desplazarse horizontalmente a izquierda y derecha, el jugador puede utilizar las teclas **A** y **D** del teclado respectivamente o las teclas de flecha **IZQUIERDA** y **DERECHA**.  Para saltar, el jugador puede usar la tecla **W**, **ESPACIO** o la tecla de flecha **ARRIBA**. Está en manos del jugador decidir qué combinación de teclas le resulta más cómoda para jugar.

El jugador empieza la partida desde el menú principal haciendo click en el botón **1 PLAYER GAME** que carga el nivel 1-1.

Una vez dentro del nivel 1-1 empieza la aventura. Por el camino el jugador tiene que enfrentarse a multitud de Goombas, los secuaces de Bowser. Estos Goombas son letales para nuestro personaje si colisionamos con ellos por los costados pero tienen un punto débil. Si el jugador aterriza encima de la cabeza del Goomba, este muere. Mario deberá ir saltando de un bloque a otro evitando caer en los precipicios que dividen el terreno. El jugador cuenta con 3 intentos para llegar al final del escenario. Cada vez que muera perderá una de sus vidas. Puede que por el camino el jugador encuentre algún power-up (setas) que convierten a Mario en... ¡Super Mario! Estas setas suelen estar escondidas dentro de bloques por todo el escenario. El jugador deberá golpear los bloques por la parte inferior para liberar las recompensas. Estas recompensas también pueden ser monedas de oro que el jugador puede ir acumulando. Tenemos un tiempo limitado para poder llegar al castillo y una vez avanzado un trozo del escenario no es posible volver.
Podemos ver una partida de prueba en [este video](https://youtu.be/jtB60G4AzGg).

## Estructura del proyecto

El proyecto está estructurado de la manera clásica de un proyecto sencillo de Unity. La carpeta **Assets** contiene todos los recursos del juego separados por tipos:

- **Animations:** contiene las animaciones de los personajes.
- **Audio:** contiene los audios del proyecto.
- **Prefabs:** contiene elementos del juego que se repiten y/o queremos generar durante runtime.
- **Scenes:** contiene las escenas del proyecto.
- **Scripts:** contiene los scripts del proyecto.
- **Sprites:** contiene los sprites del proyecto.

## Clases principales

### MenuController

Controla la lógica para iniciar la partida o salir del juego. En esta escena se inicializa la variable Lives con valor 3 como una PlayerPref. Estas propiedades se mantienen a través de las diferentes escenas y nos sirven para transmitir datos de una escena a otra.

### EndController

Muestra al jugador un mensaje de **GAME OVER** y la música de final de partida. Automáticamente redirige al jugador al menú principal pasado unos segundos.

### GameController

Es la clase principal del juego y gestiona el flujo de la partida. Actúa como interlocutor entre la UI, los elementos de la escena y las clases que gestionan los sonidos. Es el encargado también de reiniciar la escena si el jugador muere y quedan vidas, o de mostrar la escena de **GAME OVER** en caso contrario.

### CameraController

Se encarga de seguir al jugador por el escenario. Su desplazamiento es únicamente horizontal y hacia adelante (hacia la derecha). La cámara va seguida por una barrera invisible que impide al jugador volver por las partes de la escena que ya han quedado atrás (a la izquierda).

### UIController

Ofrece métodos a GameController para actualizar los diferentes campos de la UI durante la partida (Score, Coins, Level, Time y Lives).

### MainAudioController

Gestiona los sonidos de todos los elementos activos de la partida. Cada tipo de elemento (e.g.: una moneda) tiene su propio AudioSource. Esto permite que dos tipos de elementos produzcan sonidos simultáneamente y gestionarlos todos desde esta clase. Por ejemplo, es posible reproducir el sonido de Mario al saltar mientras se reproduce el de un bloque de ladrillos al romperse.

### BAckgroundMusicController

Gestiona la música de fondo. Recibe órdenes del GameController para cambiar la música cuando el tiempo se está terminando y para parar la música cuando el jugador muere o gana la partida.

### PlayerController

Contiene la lógica para gestionar los movimientos del jugador, sus animaciones y su estado interno (vidas, monedas, score, si ha comido un power-up). Se gobierna por la siguiente máquina de estados:
![alt text](https://gitlab.com/camilopalacios/pec-2---un-juego-de-plataformas/raw/master/img/player-state-machine.jpg)

### EnemyController

Contiene la lógica para gestionar los movimientos de los Goombas. Su lógica es muy sencilla. Caminan en una dirección hasta encontrar un obstáculo, en cuyo caso cambian de sentido.

### CliffController

Esta clase sirve para gestionar el final de una ronda cuando el jugador cae al vacío. Avisa al GameController para que lo gestione como una muerte.

### CliffController y FlagpoleController

Son clases para gestionar el final de una ronda. Por muerte al caer en un precipicio o por victoria al llegar al final del escenario.

### PopUpText

Sirve para mostrar valores emergentes con las puntuaciones adquiridas por el jugador.

### Blocks

Gestiona la lógica para los diferentes tipos de bloques que el jugador puede golpear: bloques sorpresa, ladrillos y sus variantes.