﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BricksController : MonoBehaviour
{

  private Animator animator;
  public GameObject debrisPrefab;
  public GameController gameController;

  // Use this for initialization
  void Start()
  {
    animator = GetComponent<Animator>();
    gameController = GameObject.Find("GameController").GetComponent<GameController>();
  }

  void OnCollisionEnter2D(Collision2D collision)
  {
    PlayerController pc = collision.gameObject.GetComponent<PlayerController>();
    // Collision on the bottom
    if (pc != null && collision.otherCollider.bounds.min.y > collision.collider.bounds.max.y)
    {

      if (pc.GetCurrentState() == PlayerController.State.small)
      {
        gameController.NotifyBricksHit();
        animator.SetTrigger("Hit");
      }
      else
      {
        gameController.NotifyBricksDestroyed();
        DestroyBlock();
      }
    }
  }

  private void DestroyBlock()
  {
    
    GameObject[] debris = new GameObject[4];
    for (int i = 0; i < 4; i++)
    {
      debris[i] = Instantiate(debrisPrefab, transform.position, transform.rotation);
    }
    debris[0].GetComponent<Rigidbody2D>().AddForce(new Vector2(20, 90));
    debris[1].GetComponent<Rigidbody2D>().AddForce(new Vector2(10, 100));
    debris[2].GetComponent<Rigidbody2D>().AddForce(new Vector2(-20, 90));
    debris[3].GetComponent<Rigidbody2D>().AddForce(new Vector2(-10, 100));
    for (int i = 0; i < 4; i++)
    {
      Destroy(debris[i], 3f);
    }
    Destroy(gameObject);
  }
}
