﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BricksMultiCoinController : MonoBehaviour
{
  private Animator animator;
  public GameObject debrisPrefab, spawn;
  private bool activated = false;
	private bool empty = false;
  private float firstHit;
  private float activeTime = 5f;

  // Use this for initialization
  void Start()
  {
    animator = GetComponent<Animator>();
  }

  void OnCollisionEnter2D(Collision2D collision)
  {
    PlayerController pc = collision.gameObject.GetComponent<PlayerController>();
    // Collision on the bottom
    if (pc != null && collision.otherCollider.bounds.min.y > collision.collider.bounds.max.y)
    {
      if (!activated)
      {
        activated = true;
        firstHit = Time.time;
      }
			if (!empty)
			{
				animator.SetTrigger("Hit");
				SpawnObject();
			}
			else {
				animator.SetBool("Empty", true);
			}
    }
  }

  private void SpawnObject()
  {
    GameObject spawnObject = Instantiate(spawn, transform.position, transform.rotation);
    spawnObject.transform.position = new Vector2(transform.position.x, transform.position.y + 0.1f);
    spawnObject.layer = LayerMask.NameToLayer("PowerUp");
		empty = IsActive();
  }

  private bool IsActive()
  {
    return Time.time >= firstHit + activeTime;
  }
}
