﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSurpriseController : MonoBehaviour
{

  public GameObject spawn;
  private Animator animator;
  private bool empty = false;

  // Use this for initialization
  void Start()
  {
    animator = GetComponent<Animator>();
  }

  void OnCollisionEnter2D(Collision2D collision)
  {
    // Collision on the bottom
    if (collision.otherCollider.bounds.min.y > collision.collider.bounds.max.y)
    {
      animator.SetBool("Hit", true);
      if (!empty)
      {
        SpawnObject();
      }
    }
  }

  private void SpawnObject()
  {
    GameObject spawnObject = Instantiate(spawn, transform.position, transform.rotation);
    spawnObject.transform.position = new Vector2(transform.position.x, transform.position.y + 0.1f);
    spawnObject.layer = LayerMask.NameToLayer("PowerUp");
    empty = true;
  }
}
