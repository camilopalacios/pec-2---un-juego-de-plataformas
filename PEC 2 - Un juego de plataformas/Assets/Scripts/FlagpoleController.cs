﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagpoleController : MonoBehaviour
{
  public GameController gameController;

  private void OnTriggerEnter2D(Collider2D other)
  {
    int finalScore = TransformIntoScore(GetComponent<Collider2D>().bounds.max.y, other.bounds.max.y);
    gameController.NotifyScore(finalScore, other.transform);
    gameController.EndGame();
  }

	private int TransformIntoScore(float topPole, float topPlayer){
    float percent = topPlayer / topPole;
    if(percent >= 0.9) return 5000;
    if(percent >= 0.5) return 2000;
    if(percent >= 0.4) return 1000;
    if(percent >= 0.3) return 500;
    if(percent >= 0.2) return 300;
    if(percent >= 0.1) return 200;
    return 100;
	}
}
