﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

  public void Start(){
    PlayerPrefs.SetInt("Lives", 3);
  }

	public void PlayGame()
  {
		SceneManager.LoadScene("Game");
  }

	public void ExitGame(){
    Debug.Log("Closing the application...");
    Application.Quit();
  }
}
