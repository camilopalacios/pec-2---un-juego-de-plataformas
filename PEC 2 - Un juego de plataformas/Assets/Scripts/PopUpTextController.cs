﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpTextController : MonoBehaviour
{
  public GameObject ui;
  public PopUpText popUpText;

  public void CreateFloatingText(string text, Transform location)
  {
    PopUpText instance = Instantiate(popUpText);
    // Vector2 screenPosition = Camera.main.WorldToScreenPoint(new Vector2(location.position.x + Random.Range(-0.5f, 0.5f), location.position.y));
		Vector2 screenPosition = Camera.main.WorldToScreenPoint(new Vector2(location.position.x, location.position.y));
    instance.transform.SetParent(ui.transform, false);
    instance.transform.position = screenPosition;
    instance.SetText(text);
  }

}
