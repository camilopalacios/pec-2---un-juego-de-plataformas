﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
  public Transform player, cameraEdgeBarrier;
  private float barrierWidth = 1f;

  // The camera will only follow the player when it tries to go from the center to the right side of the viewport
  void FixedUpdate()
  {
    if (player)
    {
      float x;
      if (player.position.x > transform.position.x) x = player.position.x;
      else if(Camera.main.ViewportToWorldPoint(new Vector3(0, 1, Camera.main.nearClipPlane)).x > player.position.x) x = player.position.x;
      else x = transform.position.x;
      transform.position = new Vector3(x, transform.position.y, transform.position.z);
      // Get the camera position in world space.
      Vector3 camPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, Camera.main.nearClipPlane));
      // Make the barrier follow the left edge of the camera
      cameraEdgeBarrier.position = new Vector3(
        camPosition.x - barrierWidth / 2,
        cameraEdgeBarrier.position.y,
        cameraEdgeBarrier.position.z
      );
    }
  }
}
