﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{
  public TextMeshProUGUI scoreText, coinsText, timeText, livesText;

  public void UpdateScore(string score)
  {
    scoreText.text = score;
  }

  public void UpdateCoins(string coins)
  {
    coinsText.text = coins;
  }

  public void UpdateTime(string time)
  {
    timeText.text = time;
  }

  public void UpdateLives(string lives)
  {
    livesText.text = lives;
  }

}
