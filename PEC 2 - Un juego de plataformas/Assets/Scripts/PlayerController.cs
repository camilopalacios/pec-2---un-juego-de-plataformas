﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  public enum State
  {
    small,
    growing,
    shrinking,
    big,
    fireSpitter,
    invincible,
    dead,
    win
  };

  private Rigidbody2D body;
  private bool facingRight = true;
  private bool playDeadMusic = true;
  private int layerMask;
  private State currentState;
  private int score, coins, lives;
  public Transform groundCheckLeft, groundCheckRight;
  public LayerMask whatIsGround;
  // TODO: make private
  public GameController gameController;
  public float groundRadius = 0.02f;
  public bool grounded;
  public Animator animator;
  [Range(1, 500)]
  public float jumpForce = 300;
  public bool jump = false;
  public bool jumping = false;
  public float runSpeed = 15f; // TODO: develop sprint ability
  public float jumpSpeed = 10f;
  public float walk = 0.0f;
  public float walkSpeed = 5f;
  public float fallMultiplier = 2.5f;
  public float lowJumpMultiplier = 2.5f;

  // Use this for initialization
  void Awake()
  {
    score = 0;
    coins = PlayerPrefs.GetInt("Coins", 0);
    lives = PlayerPrefs.GetInt("Lives", 0);

    currentState = State.small;
    body = GetComponent<Rigidbody2D>();
    animator = GetComponent<Animator>();
  }

  void Update()
  {
    ReadUserInput();
    AlterGravity();
    UpdateMovement();
  }

  public void AlterGravity()
  {
    if (body.velocity.y < 0)
    { // The player is falling
      body.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
    }
    else if (body.velocity.y > 0 && !jumping)
    {
      body.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
    }
  }

  private void ReadUserInput()
  {
    walk = Input.GetAxis("Horizontal");
    jump = Input.GetButtonDown("Jump");
    jumping = Input.GetButton("Jump");
  }

  private void UpdateMovement()
  {
    if (currentState != State.dead)
    {
      if (currentState == State.win)
      {
        body.gravityScale = 0.0f;
        body.velocity = new Vector2(0.0f, 0.0f);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("VictoryLine"), true);
        StartCoroutine(SetBackGravityAfterSeconds(1f));
      }
      else
      {
        grounded = Physics2D.OverlapCircle(groundCheckLeft.position, groundRadius, whatIsGround)
        || Physics2D.OverlapCircle(groundCheckRight.position, groundRadius, whatIsGround);
        // Update animator parameters
        animator.SetBool("Grounded", grounded);
        animator.SetFloat("Speed", Mathf.Abs(walk));

        // Apply horizontal movement
        if (!IsMotionless()) body.velocity = new Vector2(walk * walkSpeed, body.velocity.y);

        // Apply vertical movement
        if (grounded && jump)
        {
          body.AddForce(Vector2.up * jumpForce);
          gameController.NotifyJumpSmall();
        }

        // Flip sprite
        if (walk > 0.0f && !facingRight)
        {
          FlipCharacter();
        }
        else if (walk < 0.0f && facingRight)
        {
          FlipCharacter();
        }
      }
    }
  }

  private void FlipCharacter()
  {
    facingRight = !facingRight;
    Vector3 theScale = transform.localScale;
    theScale.x *= -1;
    transform.localScale = theScale;
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.layer == LayerMask.NameToLayer("PowerUp"))
    {
      gameController.NotifyPowerUpEaten();
      if (collision.gameObject.tag == "Mushroom" && currentState == State.small)
      {
        GrowCharacter();
      }
    }
    else if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
    {
      // Collision on the sides
      if (collision.otherCollider.bounds.min.y < collision.collider.bounds.max.y)
      {
        TakeDamage();
      }
    }
  }

  private void Die()
  {
    currentState = State.dead;
    body.gravityScale = 0;
    body.velocity = new Vector2(0, 0);
    animator.SetBool("Dead", true);
    // Remove collider and ground checkers
    GetComponent<Collider2D>().enabled = false;
    gameController.NotifyPlayerDead(playDeadMusic);
    playDeadMusic = false;
    StartCoroutine(PlayDeadAnimation());
    StartCoroutine(DestroyPlayer());
  }

  IEnumerator PlayDeadAnimation()
  {
    yield return new WaitForSeconds(0.5f);
    body.gravityScale = 0.5f;
    body.AddForce(new Vector2(0, 120));
  }

  IEnumerator SetBackGravityAfterSeconds(float seconds)
  {
    yield return new WaitForSeconds(seconds);
    body.gravityScale = 1f;
  }

  IEnumerator DestroyPlayer()
  {
    yield return new WaitForSeconds(2.5f);
    Destroy(gameObject);
  }

  private void TakeDamage()
  {
    if (currentState == State.small)
    {
      Die();
    }
    else if (currentState == State.big)
    {
      ShrinkCharacter();
    }
  }

  private void ShrinkCharacter()
  {
    animator.SetBool("Shrinking", true);
    transform.position = new Vector2(transform.position.x, transform.position.y - 0.04f);
    currentState = State.shrinking;
    gameController.NotifyPlayerHurt();
    Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), true);
    StartCoroutine(ResizeComponents(currentState));
  }

  private void GrowCharacter()
  {
    animator.SetBool("Growing", true);
    transform.position = new Vector2(transform.position.x, transform.position.y + 0.08f);
    body.gravityScale = 0;
    body.velocity = new Vector2(0, 0);
    currentState = State.growing;
    StartCoroutine(ResizeComponents(currentState));
  }

  IEnumerator ResizeComponents(State state)
  {
    yield return new WaitForSeconds(1);
    BoxCollider2D collider = GetComponent<BoxCollider2D>();
    collider.size = GetComponent<SpriteRenderer>().sprite.bounds.size;
    // Move the groundchecks (boots) to the new resized bottom of the collider
    foreach (Transform child in transform)
    {
      Transform boot = child.gameObject.transform;
      boot.position = new Vector2(boot.position.x, collider.bounds.min.y);
    }
    body.gravityScale = 1;

    if (state == State.growing)
    {
      currentState = State.big;
      animator.SetBool("Big", true);
      animator.SetBool("Growing", false);
    }
    else if (state == State.shrinking)
    {
      currentState = State.small;
      animator.SetBool("Big", false);
      animator.SetBool("Shrinking", false);
      Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), false);
    }
  }

  private bool IsMotionless()
  {
    return animator.GetBool("Growing");
  }

  public State GetCurrentState()
  {
    return currentState;
  }

  public int GetScore()
  {
    return score;
  }

  public void AddToScore(int aquiredScore)
  {
    score += aquiredScore;
  }

  public int GetCoins()
  {
    return coins;
  }

  public void AddToCoins(int aquiredCoins)
  {
    coins += aquiredCoins;
  }

  public int GetLives()
  {
    return lives;
  }

  public void OnDestroy()
  {
    int currentLives = lives - 1;
    PlayerPrefs.SetInt("Lives", currentLives);
    PlayerPrefs.SetInt("Coins", coins);
    gameController.NotifyPlayerDead(playDeadMusic);
  }

  public void NotifyVictory()
  {
    currentState = State.win;
  }

  public void AskPlayerToDie(){
    if(currentState != State.dead) Die();
  }
}
