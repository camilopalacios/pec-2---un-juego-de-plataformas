﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainAudioController : MonoBehaviour
{

  public AudioClip playerHurt, playerDead, coin, powerUpAppears, powerUpEaten, jumpSmall, jumpBig, bricksHit, bricksDestroyed, enemyDead;
  public AudioSource audioSourceMario, audioSourceCoin, audioSourcePowerUp, audioSourceBricks, audioSourceBlocks, audioSourceGoomba;

  private void PlaySoundEffect(AudioClip clip, AudioSource source)
  {
    if (source)
    {
      source.clip = clip;
      source.Play();
    }
  }

  public void PlayPlayerDead()
  {
    PlaySoundEffect(playerDead, audioSourceMario);
  }

	public void PlayPlayerHurt()
  {
    PlaySoundEffect(playerHurt, audioSourceMario);
  }

	public void PlayEnemyDead()
  {
    PlaySoundEffect(enemyDead, audioSourceGoomba);
  }

  public void PlayCoin()
  {
    PlaySoundEffect(coin, audioSourceCoin);
  }

  public void PlayPowerUpAppearing()
  {
    PlaySoundEffect(powerUpAppears, audioSourcePowerUp);
  }

  public void PlayPowerUpEaten()
  {
    PlaySoundEffect(powerUpEaten, audioSourcePowerUp);
  }

  public void PlayJumpSmall()
  {
    PlaySoundEffect(jumpSmall, audioSourceMario);
  }

  public void PlayJumpBig()
  {
    PlaySoundEffect(jumpBig, audioSourceMario);
  }

	public void PlayBricksHit()
  {
    PlaySoundEffect(bricksHit, audioSourceBricks);
  }

  public void PlayBricksDestroyed()
  {
    PlaySoundEffect(bricksDestroyed, audioSourceBricks);
  }
}
