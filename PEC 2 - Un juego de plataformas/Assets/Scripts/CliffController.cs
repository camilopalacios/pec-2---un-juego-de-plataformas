﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CliffController : MonoBehaviour
{
  // Destroy everything that enters the trigger
  void OnTriggerEnter2D(Collider2D other)
  {
    Destroy(other.gameObject);
  }
}
