﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
  public UIController ui;
  public PlayerController player;
  public float currentTime, maxTime = 400f;
  public PopUpTextController popUpTextController;
  public BackgroundMusicController backgroundMusic;
  public MainAudioController mainAudio;

  // Use this for initialization
  void Start()
  {
    currentTime = 0.0f;
    UpdateLivesInUI();
  }

  // Update is called once per frame
  void Update()
  {
    if (player)
    {
      ManageTime();
      UpdateScoreInUI();
      UpdateCoinsInUI();
    }
    else
    {
      ManagePlayerDead();
    }
  }

  private void ManagePlayerDead()
  {
    if (PlayerPrefs.GetInt("Lives", 0) <= 0)
    {
      SceneManager.LoadScene("GameOver");
    }
    else
    {
      StartCoroutine(RestartScene());
    }
  }

  IEnumerator RestartScene()
  {
    yield return new WaitForSeconds(2.5f);
    SceneManager.LoadScene("Game");
  }

  private void ManageTime()
  {
    int timeRemaining = (int)(maxTime - Time.timeSinceLevelLoad);
    ui.UpdateTime(timeRemaining.ToString());
		if(timeRemaining <= 0) player.AskPlayerToDie();
		else if(timeRemaining <= 100) backgroundMusic.UpdateTheme(BackgroundMusicController.MusicState.warning);
  }

  private void UpdateScoreInUI()
  {
    ui.UpdateScore(player.GetScore().ToString());
  }

  private void UpdateCoinsInUI()
  {
    ui.UpdateCoins("x " + player.GetCoins().ToString());
  }

  private void UpdateLivesInUI()
  {
    ui.UpdateLives(player.GetLives().ToString());
  }

  public void NotifyScore(int score, Transform location)
  {
    popUpTextController.CreateFloatingText(score.ToString(), location);
    player.AddToScore(score);
  }

	public void EndGame(){
		backgroundMusic.UpdateTheme(BackgroundMusicController.MusicState.victory);
		player.NotifyVictory();
		StartCoroutine(LoadEndScene());
	}

	IEnumerator LoadEndScene()
  {
    yield return new WaitForSeconds(5.5f);
    SceneManager.LoadScene("GameOver");
  }

  public void NotifyCoins(int coins)
  {
    player.AddToCoins(coins);
    mainAudio.PlayCoin();
  }

  public void NotifyPowerUpAppearing()
  {
    mainAudio.PlayPowerUpAppearing();
  }

  public void NotifyPowerUpEaten()
  {
    mainAudio.PlayPowerUpEaten();
  }

  public void NotifyJumpSmall()
  {
    mainAudio.PlayJumpSmall();
  }

  public void NotifyJumpBig()
  {
    mainAudio.PlayJumpBig();
  }

  public void NotifyBricksDestroyed()
  {
    mainAudio.PlayBricksDestroyed();
  }
  public void NotifyBricksHit()
  {
    mainAudio.PlayBricksHit();
  }

  public void NotifyPlayerDead(bool play)
  {
    backgroundMusic.StopMusic();
    if(play) mainAudio.PlayPlayerDead();
  }

	public void NotifyPlayerHurt()
  {
    mainAudio.PlayPlayerHurt();
  }

	public void NotifyEnemyDead()
  {
    mainAudio.PlayEnemyDead();
  }
}
