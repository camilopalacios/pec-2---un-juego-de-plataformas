﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndController : MonoBehaviour {
	private float resetTime = 3.5f;
	
	private void Update () {
		if(Time.timeSinceLevelLoad >= resetTime) {
			ToMainMenu();
		}
	}

	public void ToMainMenu ()
  {
		SceneManager.LoadScene("MainMenu");
  }
}
