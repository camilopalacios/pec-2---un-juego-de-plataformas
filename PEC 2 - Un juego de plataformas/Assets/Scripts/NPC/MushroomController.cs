﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomController : MonoBehaviour {
	private Rigidbody2D body;
	public float speed = 0.5f;
	public GameController gameController;
	private int worthInScore = 1000;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
		gameController = GameObject.Find("GameController").GetComponent<GameController>();
		gameController.NotifyPowerUpAppearing();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		body.velocity = new Vector2(speed, body.velocity.y);
	}

	void OnCollisionEnter2D(Collision2D collision)
  {
    if(collision.gameObject.tag == "Player"){
			gameController.NotifyScore(worthInScore, transform);
			Destroy(gameObject);
		}
		else if (collision.otherCollider.bounds.min.y < collision.collider.bounds.max.y)
    {
      ChangeDirection();
    }
  }

	private void ChangeDirection()
  {
    speed *= -1;
  }
}
