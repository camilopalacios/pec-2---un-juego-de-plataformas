﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {

	public GameController gameController;

	// Use this for initialization
	void Start () {
		gameController = GameObject.Find("GameController").GetComponent<GameController>();
		gameController.NotifyCoins(1);
		Destroy(gameObject, 0.8f);
		GetComponent<Rigidbody2D>().AddForce(Vector3.up * 130);
	}
}
