﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
  public GameController gameController;
  private Rigidbody2D body;
  private Animator animator;
  public float walkSpeed = -0.3f;
  public float delay = 02f;
  public bool alive = true;
  public bool frozen = true;
  private int worthInScore = 100;

  void Start()
  {
    gameController = GameObject.Find("GameController").GetComponent<GameController>();
    body = GetComponent<Rigidbody2D>();
    animator = GetComponent<Animator>();
    body.gravityScale = 0;
  }
  // Update is called once per frame
  void FixedUpdate()
  {
    if (frozen)
    {
      if (GetComponent<Renderer>().isVisible)
      {
        Unfreeze();
      }
    }
    else
    {
      // Apply horizontal movement
      if (alive) body.velocity = new Vector2(walkSpeed, body.velocity.y);
    }
  }

  void OnCollisionEnter2D(Collision2D collision)
  {
    // Collision on the top by the player
    if
    (
      collision.otherCollider.bounds.max.y < collision.collider.bounds.min.y
      && collision.gameObject.layer == LayerMask.NameToLayer("Player")
    )
    {
      Die();
    }
    // Collision on the sides
    else if (collision.otherCollider.bounds.min.y < collision.collider.bounds.max.y)
    {
      ChangeDirection();
    }
  }

  private void Die()
  {
    gameController.NotifyScore(worthInScore, transform);
    gameController.NotifyEnemyDead();
    animator.SetBool("Dead", true);
    alive = false;
    Destroy(body);
    Destroy(GetComponent<Collider2D>());
    Destroy(gameObject, delay);
  }

  private void ChangeDirection()
  {
    walkSpeed *= -1;
  }

  public void Unfreeze()
  {
    body.gravityScale = 1;
    frozen = false;
  }
}
