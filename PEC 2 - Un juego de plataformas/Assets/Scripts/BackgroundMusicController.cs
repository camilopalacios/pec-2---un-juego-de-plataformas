﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusicController : MonoBehaviour
{
  public enum MusicState
  {
    normal,
    warning,
		victory
  };

  public AudioClip mainTheme, hurryWarning, victoryTheme;
  public AudioSource audioSource;
	private MusicState currentState;

	void Start(){
		currentState = MusicState.normal;
	}

	public void UpdateTheme(MusicState state){
		if(currentState != state){
			if(state == MusicState.warning){
				PlaySoundEffect(hurryWarning);
				currentState = MusicState.warning;
			}
			else if(state == MusicState.victory){
				PlaySoundEffect(victoryTheme);
				currentState = MusicState.victory;
			}
		}
	}

  private void PlaySoundEffect(AudioClip clip)
  {
    audioSource.clip = clip;
    audioSource.Play();
  }

  public void StopMusic()
  {
    if (audioSource) audioSource.Stop();
  }
}
