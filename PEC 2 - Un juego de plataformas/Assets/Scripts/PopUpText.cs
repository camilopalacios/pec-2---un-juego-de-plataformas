﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopUpText : MonoBehaviour {

	public Animator animator;
	public TextMeshProUGUI text;

	public void Start(){
		AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
		Destroy(gameObject, clipInfo[0].clip.length);
	}
	public void SetText(string textValue){
		text.text = textValue;
	}
}
